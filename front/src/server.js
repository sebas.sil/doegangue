const express = require('express')
const server = express()
const nunjucks = require('nunjucks')
const sqlite3 = require('sqlite3').verbose();


//configurar o servidor para apresentar os outros arquivos
server.use(express.static('public'))
// habilita o body dos formualrios
server.use(express.urlencoded({ extended: true }))
// configura o template engine
nunjucks.configure('./', {
    express: server
})
// configura o que acontence quando o usuario requisita a raiz do sistema
server.get('/', (req, res) => {
    const db = new sqlite3.Database('banco-sangue.db3', sqlite3.OPEN_READONLY);
    db.all("SELECT id, name, email, blood FROM donors", (err, rows) =>{
        if(err) return res.send('Erro ao recuperar valores do banco de dados')
        let donors = rows
        return res.render('index.html', { donors })
    });
})

// configura o que acontece quando o usuario salva informacoes
server.post('/', (req, res) => {
    // pegar dados do formulario
    const { name, blood, email } = req.body

    if(name && blood && email){
        let donor = { name, blood, email }
        //configura a conexao com o banco de dados
        const db = new sqlite3.Database('banco-sangue.db3', sqlite3.OPEN_READWRITE);
        let stmt = db.prepare("INSERT INTO donors (name, email, blood) VALUES (?,?,?)");
        stmt.run([donor.name, donor.email, donor.blood], (err) => {
            if(err) return res.send('Erro ao gravar valores no banco de dados')
            db.close();
            res.redirect('/')
        });
        stmt.finalize();
        
    } else {
        console.log('Campos obrigatorios nao preenchidos')
        res.redirect('/')
    }
})

// inicia o servidor na porta 3000
server.listen(3000, () => {
    console.log('servidor iniciado na parta 3000')
})